package es.cic.curso.curso14.mascota.controller;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso14.mascota.dominio.ConverterEntrenadorImpl;
import es.cic.curso.curso14.mascota.dominio.Entrenador;
import es.cic.curso.curso14.mascota.dominio.EntrenadorDTO;
import es.cic.curso.curso14.mascota.helper.TestHelper;
import es.cic.curso.curso14.mascota.repository.EntrenadorRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso14/mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EntrenadorControllerTest {
	
	@Autowired
	private EntrenadorController entrenadorController;
	@Autowired
	private EntrenadorRepository entrenadorRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private ConverterEntrenadorImpl converterEntrenador;
	
	private Long claveEntrenador;
	private EntrenadorDTO entrenadorDTO;

	@Before
	public void setUp() throws Exception {
		claveEntrenador = tHelper.generaEntrenador();
		
		entrenadorDTO = converterEntrenador.converToDTO(entrenadorRepository.read(claveEntrenador));
	}

	@Test
	public void testNuevoEntrenador() {
		EntrenadorDTO entrenadorNuevo = entrenadorController.nuevoEntrenador(entrenadorDTO);
		
		Entrenador entrenadorAniadido = entrenadorRepository.read(entrenadorNuevo.getId());
		
		assertEquals("Gonchy", entrenadorAniadido.getNombreEntrenador());
	}
	
	@Test
	public void testModificarEntrenador() {
		EntrenadorDTO entrenadorNuevo = entrenadorController.modificarEntrenador(entrenadorDTO, "Javier");
		
		Entrenador entrenadorAniadido = entrenadorRepository.read(entrenadorNuevo.getId());
		
		assertEquals("Javier", entrenadorAniadido.getNombreEntrenador());
	}
	
	@Test
	public void testBorrarEntrenador() {
		List<Entrenador> listaEntrenadores = entrenadorRepository.list();
		assertEquals(1, listaEntrenadores.size());
		
		entrenadorController.borrarEntrenador(entrenadorDTO);
		
		listaEntrenadores = entrenadorRepository.list();

		assertEquals(0, listaEntrenadores.size());
	}
	
	@Test
	public void testListarEntrenadores() {
		Collection<EntrenadorDTO> listaEntrenadores = entrenadorController.listarEntrenadores();
		
		assertEquals(1, listaEntrenadores.size());
		
		claveEntrenador = tHelper.generaEntrenador();
		Entrenador entrenadorResultado = entrenadorRepository.read(claveEntrenador);
		entrenadorRepository.add(entrenadorResultado);
		
		claveEntrenador = tHelper.generaEntrenador();
		entrenadorResultado = entrenadorRepository.read(claveEntrenador);
		entrenadorRepository.add(entrenadorResultado);
		
		listaEntrenadores = entrenadorController.listarEntrenadores();
		
		assertEquals(3, listaEntrenadores.size());
	}
	
	@Test
	public void testBorrarEntrenador_Integracion() {
		Collection<EntrenadorDTO> listaEntrenadores = entrenadorController.listarEntrenadores();
		assertEquals(1, listaEntrenadores.size());
		
		entrenadorController.borrarEntrenador(entrenadorDTO);
		
		listaEntrenadores = entrenadorController.listarEntrenadores();

		assertEquals(0, listaEntrenadores.size());
	}
}