package es.cic.curso.curso14.mascota.controller;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso14.mascota.dominio.ConverterJugadorImpl;
import es.cic.curso.curso14.mascota.dominio.Equipo;
import es.cic.curso.curso14.mascota.dominio.Jugador;
import es.cic.curso.curso14.mascota.dominio.JugadorDTO;
import es.cic.curso.curso14.mascota.helper.TestHelper;
import es.cic.curso.curso14.mascota.repository.EquipoRepository;
import es.cic.curso.curso14.mascota.repository.JugadorRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso14/mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class JugadorControllerTest {
	
	@Autowired
	private JugadorController jugadorController;
	@Autowired
	private JugadorRepository jugadorRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private ConverterJugadorImpl converterJugador;
	@Autowired
	private EquipoRepository equipoRepository;
	
	private Long claveJugador;
	private JugadorDTO jugadorDTO;
	
	private Long claveEquipo;
	private Equipo equipo;

	@Before
	public void setUp() throws Exception {
		claveEquipo = tHelper.generaEquipo();
		equipo = equipoRepository.read(claveEquipo);
		
		claveJugador = tHelper.generaJugadorLocal(equipo);
		jugadorDTO = converterJugador.converToDTO(jugadorRepository.read(claveJugador));
	}

	@Test
	public void testNuevoJugador() {
		JugadorDTO jugadorNuevo = jugadorController.nuevoJugador(jugadorDTO);
		
		Jugador jugadorAniadido = jugadorRepository.read(jugadorNuevo.getId());
		
		assertEquals("Vic", jugadorAniadido.getNombreJugador());
		assertEquals(14, jugadorAniadido.getDorsal());
		assertEquals(true, jugadorAniadido.isCapitan());
	}

	@Test
	public void testModificarJugador() {
		JugadorDTO jugadorNuevo = jugadorController.modificarJugador(jugadorDTO, jugadorDTO.getNombreEquipo(), "Enia", 7, false);
				
		Jugador jugadorAniadido = jugadorRepository.read(jugadorNuevo.getId());
		
		assertEquals("Enia", jugadorAniadido.getNombreJugador());
		assertEquals(7, jugadorAniadido.getDorsal());
		assertEquals(false, jugadorAniadido.isCapitan());
	}
	
	@Test
	public void testBorrarJugador_Integracion() {
		Collection<JugadorDTO> listaJugadores = jugadorController.listarJugadores();
		
		assertEquals(1, listaJugadores.size());
		
		jugadorController.borrarJugador(jugadorDTO);
		
		listaJugadores = jugadorController.listarJugadores();

		assertEquals(0, listaJugadores.size());
	}
}