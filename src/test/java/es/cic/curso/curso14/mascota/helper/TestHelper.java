package es.cic.curso.curso14.mascota.helper;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.cic.curso.curso14.mascota.dominio.Entrenador;
import es.cic.curso.curso14.mascota.dominio.Equipo;
import es.cic.curso.curso14.mascota.dominio.Estadistica;
import es.cic.curso.curso14.mascota.dominio.Jugador;
import es.cic.curso.curso14.mascota.dominio.Partido;
import es.cic.curso.curso14.mascota.repository.EntrenadorRepository;
import es.cic.curso.curso14.mascota.repository.JugadorRepository;
import es.cic.curso.curso14.mascota.repository.PartidoRepository;

@Repository
public class TestHelper {
	
	@Autowired
	private EntrenadorRepository entrenadorRepository;
	@Autowired
	private PartidoRepository partidoRepository;
	@Autowired
	private JugadorRepository jugadorRepository;
	
	
	@PersistenceContext
	private EntityManager em;
	
	public Long generaEntrenador() {
		Entrenador en = new Entrenador();
		en.setNombreEntrenador("Gonchy");
		
		em.persist(en);
		return en.getId();
	}
	
	public Long generaEquipo() {
		Equipo eq = new Equipo();
		eq.setEntrenador(entrenadorRepository.read(generaEntrenador()));
		eq.setNombreEquipo("Daygon");
		
		em.persist(eq);
		return eq.getId();
	}
	
	public Long generaJugadorLocal(Equipo equipoLocal) {
		Jugador j = new Jugador();
		j.setEquipo(equipoLocal);
		j.setNombreJugador("Vic");
		j.setDorsal(14);
		j.setCapitan(true);
		
		em.persist(j);
		return j.getId();
	}
	
	public Long generaJugadorVisitante(Equipo equipoVisitante) {
		Jugador j = new Jugador();
		j.setEquipo(equipoVisitante);
		j.setNombreJugador("Vic");
		j.setDorsal(14);
		j.setCapitan(true);
		
		em.persist(j);
		return j.getId();
	}
	
	public Long generaEstadisticaLocal(Equipo equipoLocal, Equipo equipoVisitante) {
		Estadistica es = new Estadistica();
		es.setPartido(partidoRepository.read(generaPartido(equipoLocal, equipoVisitante)));
		es.setJugador(jugadorRepository.read(generaJugadorLocal(equipoLocal)));
		es.setTirosDos(0);
		es.setTirosDosAnotados(0);
		es.setTirosDosPorcentaje(0);
		es.setTirosTriples(0);
		es.setTirosTriplesAnotados(0);
		es.setTirosTriplesPorcentaje(0);
		es.setTirosLibres(0);
		es.setTirosLibresAnotados(0);
		es.setTirosLibresPorcentaje(0);
		es.setFaltasCometidas(0);
		es.setFaltasRecibidas(2);
		es.setValoracion(0);
		
		em.persist(es);
		return es.getId();
	}
	
	public Long generaEstadisticaVisitante(Equipo equipoLocal, Equipo equipoVisitante) {
		Estadistica es = new Estadistica();
		es.setPartido(partidoRepository.read(generaPartido(equipoLocal, equipoVisitante)));
		es.setJugador(jugadorRepository.read(generaJugadorLocal(equipoVisitante)));
		es.setTirosDos(0);
		es.setTirosDosAnotados(0);
		es.setTirosDosPorcentaje(0);
		es.setTirosTriples(0);
		es.setTirosTriplesAnotados(0);
		es.setTirosTriplesPorcentaje(0);
		es.setTirosLibres(0);
		es.setTirosLibresAnotados(0);
		es.setTirosLibresPorcentaje(0);
		es.setFaltasCometidas(0);
		es.setFaltasRecibidas(2);
		es.setValoracion(0);
		
		em.persist(es);
		return es.getId();
	}
	
	public Long generaPartido(Equipo equipoLocal, Equipo equipoVisitante) {
		Partido p = new Partido();
		p.setEquipoLocal(equipoLocal);
		p.setEquipoVisitante(equipoVisitante);
		p.setPuntosLocal(0);
		p.setPuntosVisitante(0);
		p.setTmLocal(3);
		p.setTmVisitante(3);
		p.setFcLocal(0);
		p.setFcVisitante(0);
		p.setFtLocal(0);
		p.setFtVisitante(0);
		p.setPosesionLocal(true);
		
		em.persist(p);
		return p.getId();
	}
}