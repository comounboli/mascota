package es.cic.curso.curso14.mascota.repository;

import org.springframework.stereotype.Repository;

import es.cic.curso.curso14.mascota.dominio.Equipo;

@Repository
public class EquipoRepositoryImpl extends AbstractRepositoryImpl<Long, Equipo> implements EquipoRepository {

	@Override
	public Class<Equipo> getClassDeT() {
		return Equipo.class;
	}

	@Override
	public String getNombreTabla() {
		return "EQUIPO";
	}
}