package es.cic.curso.curso14.mascota.repository;

import org.springframework.stereotype.Repository;

import es.cic.curso.curso14.mascota.dominio.Jugador;

@Repository
public class JugadorRepositoryImpl extends AbstractRepositoryImpl<Long, Jugador> implements JugadorRepository {

	@Override
	public Class<Jugador> getClassDeT() {
		return Jugador.class;
	}

	@Override
	public String getNombreTabla() {
		return "JUGADOR";
	}
}