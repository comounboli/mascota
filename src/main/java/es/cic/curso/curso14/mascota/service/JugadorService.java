package es.cic.curso.curso14.mascota.service;

import java.io.Serializable;
import java.util.List;

import es.cic.curso.curso14.mascota.dominio.Equipo;
import es.cic.curso.curso14.mascota.dominio.Jugador;

public interface JugadorService extends Serializable{

	boolean borrarJugador(long jugadorId);

	List<Jugador> listarJugadores();

	Jugador nuevoJugador(Equipo equipoId, String nombre, int dorsal, boolean capitan);

	Jugador modificarJugador(long jugadorId, Equipo equipoId, String nombre, int dorsal, boolean capitan);
}