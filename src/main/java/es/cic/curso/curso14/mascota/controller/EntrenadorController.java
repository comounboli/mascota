package es.cic.curso.curso14.mascota.controller;

import java.util.Collection;

import es.cic.curso.curso14.mascota.dominio.EntrenadorDTO;

public interface EntrenadorController {

	EntrenadorDTO nuevoEntrenador(EntrenadorDTO entrenadorDTO);

	EntrenadorDTO modificarEntrenador(EntrenadorDTO entrenadorDTO, String nombre);

	void borrarEntrenador(EntrenadorDTO entrenadorDTO);

	Collection<EntrenadorDTO> listarEntrenadores();
}