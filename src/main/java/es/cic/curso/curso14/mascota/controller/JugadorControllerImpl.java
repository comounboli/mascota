package es.cic.curso.curso14.mascota.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import es.cic.curso.curso14.mascota.dominio.ConverterJugadorImpl;
import es.cic.curso.curso14.mascota.dominio.Jugador;
import es.cic.curso.curso14.mascota.dominio.JugadorDTO;
import es.cic.curso.curso14.mascota.repository.EquipoRepository;
import es.cic.curso.curso14.mascota.service.JugadorService;

@Controller
public class JugadorControllerImpl implements JugadorController, Serializable{

	private static final long serialVersionUID = -3112077466379384049L;
	
	@Autowired
	private ConverterJugadorImpl converterJugador;
	@Autowired 
	private JugadorService jugadorService;
	@Autowired
	private EquipoRepository equipoRepository;
	
	@Override
	public JugadorDTO nuevoJugador (JugadorDTO jugadorDTO){
		Jugador jugador = converterJugador.converToENTITY(jugadorDTO);
				
		Jugador nuevoJugador = jugadorService.nuevoJugador(jugador.getEquipo(), jugadorDTO.getNombreJugador(), jugadorDTO.getDorsal(), jugadorDTO.isCapitan());
		
		return converterJugador.converToDTO(nuevoJugador);
	}

	@Override
	public JugadorDTO modificarJugador(JugadorDTO jugadorDTO, String nombreEquipo, String nombreJugador, int dorsal, boolean capitan){
		jugadorDTO.setNombreEquipo(nombreEquipo);
		Jugador jugador = converterJugador.converToENTITY(jugadorDTO);
		
		Jugador nuevoJugador = jugadorService.modificarJugador(jugadorDTO.getId(), jugador.getEquipo(), nombreJugador, dorsal, capitan);
		
		return converterJugador.converToDTO(nuevoJugador);	
	}
	
	@Override
	public void borrarJugador(JugadorDTO jugadorDTO){
		Jugador jugador = converterJugador.converToENTITY(jugadorDTO);
		
		jugadorService.borrarJugador(jugador.getId());
	}

	@Override
	public Collection<JugadorDTO> listarJugadores(){
		Collection<Jugador> jugador = jugadorService.listarJugadores();
		Collection<JugadorDTO> resultado = new ArrayList<>();
		
		for (Jugador j: jugador) {
			resultado.add(converterJugador.converToDTO(j));
		}
		return resultado;
	}
}