package es.cic.curso.curso14.mascota.repository;

import org.springframework.stereotype.Repository;

import es.cic.curso.curso14.mascota.dominio.Estadistica;

@Repository
public class EstadisticaRepositoryImpl extends AbstractRepositoryImpl<Long, Estadistica> implements EstadisticaRepository {

	@Override
	public Class<Estadistica> getClassDeT() {
		return Estadistica.class;
	}

	@Override
	public String getNombreTabla() {
		return "ESTADISTICA";
	}
}