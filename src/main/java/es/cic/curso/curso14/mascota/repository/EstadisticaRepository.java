package es.cic.curso.curso14.mascota.repository;

import java.io.Serializable;

import es.cic.curso.curso14.mascota.dominio.Estadistica;

public interface EstadisticaRepository extends IRepository<Long, Estadistica>, Serializable{
}