package es.cic.curso.curso14.mascota.controller;

import java.util.Collection;

import es.cic.curso.curso14.mascota.dominio.JugadorDTO;

public interface JugadorController {

	JugadorDTO nuevoJugador(JugadorDTO jugadorDTO);

	void borrarJugador(JugadorDTO jugadorDTO);

	Collection<JugadorDTO> listarJugadores();

	JugadorDTO modificarJugador(JugadorDTO jugadorDTO, String nombreEquipo, String nombreJugador, int dorsal,
			boolean capitan);
}