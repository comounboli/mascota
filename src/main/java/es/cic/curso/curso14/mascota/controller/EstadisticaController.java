package es.cic.curso.curso14.mascota.controller;

import java.util.Collection;

import es.cic.curso.curso14.mascota.dominio.EstadisticaDTO;

public interface EstadisticaController {

	EstadisticaDTO sumarDosAnotado(EstadisticaDTO estadisticaDTO);

	EstadisticaDTO sumarDosFallado(EstadisticaDTO estadisticaDTO);

	EstadisticaDTO sumarTripleFallado(EstadisticaDTO estadisticaDTO);

	EstadisticaDTO sumarTripleAnotado(EstadisticaDTO estadisticaDTO);

	EstadisticaDTO sumarLibreAnotado(EstadisticaDTO estadisticaDTO);

	EstadisticaDTO sumarLibreFallado(EstadisticaDTO estadisticaDTO);

	EstadisticaDTO sumarFaltaCometida(EstadisticaDTO estadisticaDTO);

	EstadisticaDTO sumarFaltaRecibida(EstadisticaDTO estadisticaDTO);

	int obtenerValoracion(EstadisticaDTO estadisticaDTO);

	Collection<EstadisticaDTO> listarEstadisticas();

	EstadisticaDTO nuevaEstadisticaEnBlanco(long partidoId, long jugadorId);
}