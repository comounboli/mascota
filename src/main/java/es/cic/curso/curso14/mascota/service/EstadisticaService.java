package es.cic.curso.curso14.mascota.service;

import java.io.Serializable;
import java.util.Collection;

import es.cic.curso.curso14.mascota.dominio.Estadistica;

public interface EstadisticaService extends Serializable{
	
	Estadistica sumarDosAnotado(Estadistica estadistica);

	Estadistica sumarDosFallado(Estadistica estadistica);
	
	Estadistica sumarTripleAnotado(Estadistica estadistica);

	Estadistica sumarTripleFallado(Estadistica estadistica);
	
	Estadistica sumarLibreAnotado(Estadistica estadistica);

	Estadistica sumarLibreFallado(Estadistica estadistica);

	Estadistica sumarFaltaCometida(Estadistica estadistica);

	Estadistica sumarFaltaRecibida(Estadistica estadistica);

	int obtenerValoracion(Estadistica estadistica);

	Collection<Estadistica> listarEstadisticas();

	Estadistica nuevaEstadisticaEnBlanco(long partidoId, long jugadorId);
}