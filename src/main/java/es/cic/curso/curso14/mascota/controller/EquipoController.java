package es.cic.curso.curso14.mascota.controller;

import java.util.Collection;

import es.cic.curso.curso14.mascota.dominio.EquipoDTO;

public interface EquipoController {

	EquipoDTO nuevoEquipo(EquipoDTO equipoDTO);

	void borrarEquipo(EquipoDTO equipoDTO);

	Collection<EquipoDTO> listarEquipos();

	EquipoDTO modificarEquipo(EquipoDTO equipoDTO, String nombreEntrenador, String nombreEquipo);
}