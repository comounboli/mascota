package es.cic.curso.curso14.mascota.controller;

import java.util.Collection;

import es.cic.curso.curso14.mascota.dominio.PartidoDTO;

public interface PartidoController {

	boolean borrarPartido(PartidoDTO partidoDTO);

	Collection<PartidoDTO> listarPartidos();

	boolean sumarFaltaTecnicaEntrenador(PartidoDTO partidoDTO, long entrenadorId);

	PartidoDTO nuevoPartidoEnBlanco(long equipoLocalId, long equipoVisitanteId);

	PartidoDTO modificarPartido(PartidoDTO partidoDTO, long equipoLocalId, long equipoVisitanteId);

	boolean pedirTM(PartidoDTO partidoDTO, long equipoId);

	PartidoDTO cambiarPosesion(PartidoDTO partidoDTO);
}