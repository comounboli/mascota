package es.cic.curso.curso14.mascota.repository;

import java.io.Serializable;

import es.cic.curso.curso14.mascota.dominio.Entrenador;

public interface EntrenadorRepository extends IRepository<Long, Entrenador>, Serializable{
}