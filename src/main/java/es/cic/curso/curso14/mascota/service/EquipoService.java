package es.cic.curso.curso14.mascota.service;

import java.io.Serializable;
import java.util.List;

import es.cic.curso.curso14.mascota.dominio.Entrenador;
import es.cic.curso.curso14.mascota.dominio.Equipo;

public interface EquipoService extends Serializable{

	Equipo nuevoEquipo(Entrenador entrenador, String nombre);
	
	boolean borrarEquipo(long equipoId);

	List<Equipo> listarEquipos();

	Equipo modificarEquipo(long equipoId, Entrenador entrenador, String nombre);
}