package es.cic.curso.curso14.mascota.repository;

import java.io.Serializable;

import es.cic.curso.curso14.mascota.dominio.Jugador;

public interface JugadorRepository extends IRepository<Long, Jugador>, Serializable{
}