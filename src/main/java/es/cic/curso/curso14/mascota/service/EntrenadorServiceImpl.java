package es.cic.curso.curso14.mascota.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso14.mascota.dominio.Entrenador;
import es.cic.curso.curso14.mascota.repository.EntrenadorRepository;

@Service
@Transactional
public class EntrenadorServiceImpl implements EntrenadorService {
	
	@Autowired 
	private EntrenadorRepository entrenadorRepository;

	@Override
	public Entrenador nuevoEntrenador(String nombre){
		Entrenador nuevoEntrenador = new Entrenador();
		
		nuevoEntrenador.setNombreEntrenador(nombre);
		
		entrenadorRepository.add(nuevoEntrenador);
		
		return nuevoEntrenador;
	}
	
	@Override
	public Entrenador modificarEntrenador(long entrenadorId,String nombre){
		Entrenador modificadoEntrenador = entrenadorRepository.read(entrenadorId);

		modificadoEntrenador.setNombreEntrenador(nombre);

		entrenadorRepository.update(modificadoEntrenador);
		return modificadoEntrenador;
	}
	
	@Override
	public boolean borrarEntrenador(long entrenadorId){
		boolean resultado = false;
		
		for (Entrenador e : entrenadorRepository.list()){
			if(e.getId() == entrenadorId){
				entrenadorRepository.delete(e);
				resultado = true;
			}
		}
		return resultado;
	}
	
	@Override
	public List<Entrenador> listarEntrenadores(){
		return entrenadorRepository.list();
	}
}