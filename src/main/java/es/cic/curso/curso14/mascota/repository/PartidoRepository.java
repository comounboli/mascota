package es.cic.curso.curso14.mascota.repository;

import java.io.Serializable;

import es.cic.curso.curso14.mascota.dominio.Partido;

public interface PartidoRepository extends IRepository<Long, Partido>, Serializable{
}