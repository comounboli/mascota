package es.cic.curso.curso14.mascota.repository;

import java.io.Serializable;

import es.cic.curso.curso14.mascota.dominio.Equipo;

public interface EquipoRepository extends IRepository<Long, Equipo>, Serializable{
}