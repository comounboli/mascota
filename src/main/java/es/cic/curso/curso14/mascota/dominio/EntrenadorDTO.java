package es.cic.curso.curso14.mascota.dominio;

public class EntrenadorDTO {
	
	private Long id;
	
	private String nombreEntrenador;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreEntrenador() {
		return nombreEntrenador;
	}

	public void setNombreEntrenador(String nombreEntrenador) {
		this.nombreEntrenador = nombreEntrenador;
	}
}